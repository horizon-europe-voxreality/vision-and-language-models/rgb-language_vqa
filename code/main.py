from transformers import BlipProcessor, BlipForQuestionAnswering
from fastapi import FastAPI, UploadFile
from pydantic import BaseModel
from datetime import datetime
import os
import csv
import shutil
import torch
import demo
import time
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["POST", "GET"],
    allow_headers=["*"],
)

# load models and model components
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
model_checkpoint = "blip_model"
processor = BlipProcessor.from_pretrained(model_checkpoint)
model = BlipForQuestionAnswering.from_pretrained(model_checkpoint).to(device)

class Questions(BaseModel):
    file: UploadFile
    questions: str

@app.get("/")
async def read_root() -> dict:
    return {"message": "Welcome to Certh's API! \nAppend to /docs to start using the api!"}

@app.post("/rgb-language_vqa/")
async def create_upload_file(file:UploadFile, question: str):

    #create a new folder with the current date and time
    now = datetime.now()
    date_time = now.strftime("%Y%m%d-%H%M%S")
    folder_name = now.strftime("%Y-%m-%d")
    folder_path = os.path.join("VQA", folder_name)
    os.makedirs(folder_path, exist_ok=True)
    
    #Split the filename and extension
    filename, ext = os.path.splitext(file.filename)
    new_filename = f"{filename}-{date_time}{ext}"
    
    #save the uploaded file to the new folder
    file_path = os.path.join(folder_path, new_filename)
    with open(file_path, "wb") as buffer:
       shutil.copyfileobj(file.file, buffer)
    
    t1 = time.time()
    #split the questions and get the answers
    pred_answers = demo.vqa(file_path, question, processor, model)
    t2 = time.time()
    total_t = t2-t1

    csv_path = os.path.join(folder_path, f"{filename}-{date_time}.csv")
    with open(csv_path, mode='w', newline='') as csvfile:
        fieldnames = ['Image', 'Question', 'Answer', 'Inference_time_sec']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerow({'Image': new_filename, 'Question': question, 'Answer': pred_answers[0][0], 'Inference_time_sec': total_t})
        
    return ({"Prediction": pred_answers})
