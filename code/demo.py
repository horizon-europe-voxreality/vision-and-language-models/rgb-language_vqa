import time
from transformers import LxmertForQuestionAnswering, LxmertTokenizer
import torch
from PIL import Image

def vqa(file_path, question, processor, model):
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    start_time = time.time()

    pred_answers = []
    image = Image.open(file_path).convert("RGB")
    encoding = processor(image, question, return_tensors="pt").to(device)
    out = model.generate(**encoding, max_new_tokens=200)
    pred_answers.append(processor.decode(out[0], skip_special_tokens=True))
        
    end_time = time.time()

    elapsed_time = end_time - start_time
    print(f"Elapsed time: {elapsed_time:.2f} seconds")

    return ([pred_answers])
