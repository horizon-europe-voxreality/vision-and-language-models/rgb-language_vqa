# Welcome to the VOXReality Horizon Europe Project 

Below you'll find the necessary instructions in order to run our provided code. The instructions refer to the building of the rgb-language_vqa service which exposes 1 endpoint and utilizes the VOXReality vision-language Visual Spatial Question Answering model.


## 1. Requirements
---
1. CUDA compatible GPU. 
   1. We recommend at least 4GB of GPU memory.
   2. The code was tested on Nvidia proprietary driver 515 and 525.
2. For LINUX (tested on Ubuntu 20.04).
   1. Make sure Docker is installed on your system.
   2. Make sure you have the NVIDIA Container Toolking installed. More info and instructions can be found in the [official installation guide](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker)
3. For Windows (tested on Windows 10 and 11).
   1. Make sure Docker is installed on your system.


One you have docker up and running you can move to cloning the repository.

## 2. Cloning the repository
---
1. Start by cloning the repository by running the following command:
   `git clone https://gitlab.com/horizon-europe-voxreality/vision-and-language-models/rgb-language_vqa.git`
   
## 3. Building the docker image
---
In order to build the docker image from scratch you should follow the instructions below:

1. To build the docker you need to have already downloaded the model(`https://huggingface.co/voxreality/rgb_language_vqa`) at `code\blip_model`.
2. Open a terminal in the same folder where the dockerfile resides and execute `docker build .`

## 4. Running the docker images
---
1. You can start the docker container by running the `LINUX_Start_Contrainer.sh` or the `WIN_GPU_Start_Container.bat` script respectively.
2. There should be a message in the terminal that will display whether the GPU is accessed by the container or not. If the GPU is available it will use it, else it will run on CPU. If for some reason you want to run it manually with CPU you can run 'sudo docker run -it -p 5041:5041 --network host --name voxreality_rgb-language_vqa voxreality/rgb-language:vqa'.
3. Once the container has started, you can move to the `https://localhost:5041/docs` or `https://YourExternalIP:5041/docs` address on your browser and start using the api.
4. To stop the container run the `LINUX_Stop_Container.sh` or the `WIN_GPU_Stop_Container.bat` script respectively. This script will create a `Dumps` folder and copy all the tests you have run e.g. uploaded photos and generated captions.


## 5. Example of Usage
---
Once you start the container, you can use the service at localhost:5041/docs.

Click the /rgb-language-vqa/ and then the button "try it out". Now you can ask a question and upload an image to test the service.
Image could be either .png or .jpg. Press execute and scroll down the page and there will be a response body with a prediction response.
Response will vary based on the question. You can ask questions like "what is there", "how many objects are there", "where is this A"(Where A is the desired object), "what is to the left/right etc of A" etc.

![Logo](image.jpg)

## 6. Expected output
---
Q: what is there?
A:	{
  "Prediction": [
    [
      "a person, a tennis racket and a sports ball."
    ]
  ]
}