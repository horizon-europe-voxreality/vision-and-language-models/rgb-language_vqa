#!/bin/bash
sudo docker stop voxreality_rgb-language_vqa
FOLDER=$(date +%Y_%m_%d_%r)
mkdir -p Dumps
sudo docker cp "voxreality_rgb-language_vqa:/code/VQA" "./Dumps/$FOLDER"
sudo docker rm voxreality_rgb-language_vqa
