setlocal enabledelayedexpansion

docker stop voxreality_rgb-language_vqa
set FOLDER=%DATE:/=_%_%TIME::=_%
mkdir "Dumps\%FOLDER%"
docker cp voxreality_rgb-language_vqa:/code/VQA "Dumps\%FOLDER%"
docker rm voxreality_rgb-language_vqa
pause